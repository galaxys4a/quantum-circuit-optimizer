from optimizer import QuantumCircuitOptimizer


# edges = [
#     (0, 1),
#     (1, 4),
#     (1, 2),
#     (4, 7),
#     (6, 7),
#     (7, 10),
#     (10, 12),
#     (12, 15),
#     (12, 13),
#     (13, 14),
#     (2, 3),
#     (3, 5),
#     (5, 8),
#     (8, 9),
#     (8, 11),
#     (11, 14),
# ]

# edges = [(0, 1), (1, 2), (2, 3)]

ibmq_lima = [(0, 1), (1, 2), (1, 3), (3, 4)]
ibmq_manila = [(0, 1), (1, 2), (2, 3), (3, 4)]
ibmq_jacarta = [(0, 1), (1, 2), (1, 3), (3, 5), (4, 5), (5, 6)]


if __name__ == "__main__":
    n = 7
    edges = ibmq_jacarta
    print("IBMQ_JACARTA")
    optimizer = QuantumCircuitOptimizer(n ,edges)
    optimizer.optimize()
