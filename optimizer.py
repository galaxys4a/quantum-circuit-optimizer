from typing import List, Tuple

from graph import Graph

from sympy.combinatorics import GrayCode


class QuantumCircuitOptimizer:
    def __init__(self, n: int, edges: List[Tuple[int, int]]):
        self.n = n
        self.gray = None
        self.graph = Graph(n, edges)
        self.graph.dijkstra()

    def optimize(self) -> None:
        self.gray = GrayCode(self.n - 1)
        for target in range(self.n):
            cnots = self._get_cnots_count(target)
            print(f"target = {target + 1}: {cnots} CNOTs\n")

    def _get_cnots_count(self, target: int) -> int:
        cnots = 0
        for i in range(2 ** (self.n - 1)):
            control = self._get_control_index(i, target)
            count = self._get_nearest_neighbor_cnots_count(control, target)
            # print(control, count)
            cnots += count
        return cnots

    def _get_control_index(self, current: int, target: int) -> int:
        cur = self.gray.next(current)
        next = self.gray.next(current + 1)
        diff = self._get_diff_bit(cur.current, next.current, target)
        return diff

    def _get_diff_bit(self, current: str, next: str, target: int) -> int:
        for i in range(len(current)):
            if current[i] != next[i]:
                diff = i
                break
        if diff == target:
            diff = self.n - 1
        return diff

    def _get_nearest_neighbor_cnots_count(self, control: int, target: int) -> int:
        l = self._get_path_length(control, target)
        return 2 * (l - 1) + 1

    def _get_path_length(self, control: int, target: int) -> int:
        # print("CONTROL TARGET PATH", control, target, self.graph.get_distance(control, target))
        return self.graph.get_distance(control, target)
