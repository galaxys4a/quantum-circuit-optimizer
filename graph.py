class Graph:
    def __init__(self, vertices, edges):
        self.V = vertices
        self.graph = [[0 for column in range(vertices)] for row in range(vertices)]
        self._set_edges(edges)
        self.distances = None

    def _set_edges(self, edges):
        for edge in edges:
            i = edge[0]
            j = edge[1]
            self.graph[i][j] = 1
            self.graph[j][i] = 1

    def minDistance(self, dist, sptSet):
        min = 1e7
        for v in range(self.V):
            if dist[v] < min and sptSet[v] == False:
                min = dist[v]
                min_index = v
        return min_index

    def _dijkstra(self, src):
        dist = [1e7] * self.V
        dist[src] = 0
        sptSet = [False] * self.V
        for cout in range(self.V):
            u = self.minDistance(dist, sptSet)
            sptSet[u] = True
            for v in range(self.V):
                if (
                    self.graph[u][v] > 0
                    and sptSet[v] == False
                    and dist[v] > dist[u] + self.graph[u][v]
                ):
                    dist[v] = dist[u] + self.graph[u][v]
        return dist

    def dijkstra(self):
        self.distances = [[] for _ in range(self.V)]
        for v in range(self.V):
            dist = self._dijkstra(v)
            self.distances[v] = dist

    def get_distance(self, i: int, j: int) -> int:
        return self.distances[i][j]
